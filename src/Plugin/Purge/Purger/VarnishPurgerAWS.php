<?php

namespace Drupal\varnish_purger_aws\Plugin\Purge\Purger;

use Drupal\varnish_purger\Plugin\Purge\Purger\VarnishPurger;
use Drupal\varnish_purger_aws\Ec2Finder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @PurgePurger(
 *   id = "varnish_aws",
 *   label = @Translation("Varnish Purger AWS"),
 *   configform = "\Drupal\varnish_purger_aws\Form\VarnishPurgerAWSForm",
 *   cooldown_time = 0.0,
 *   description = @Translation("Configurable purger that makes HTTP requests for each given invalidation instruction."),
 *   multi_instance = TRUE,
 *   types = {},
 * )
 */
class VarnishPurgerAWS extends VarnishPurger {

  protected Ec2Finder $ec2Finder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->ec2Finder = $container->get('varnish_purger_aws.ec2_finder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    $ips = $this->ec2Finder->getEc2Ips($this->settings);

    if (!empty($ips)) {
      foreach ($ips as $ip) {
        foreach ($invalidations as $invalidation) {
          $invalidation->setProperty('aws_ip', $ip);
        }
        parent::invalidate($invalidations);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getUri($token_data) {
    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    $invalidation = $token_data['invalidation'];
    $ip = $invalidation->getProperty('aws_ip');
    return sprintf(
      '%s://%s:%s%s',
      $this->settings->scheme,
      $ip,
      $this->settings->port,
      $this->token->replace($this->settings->path, $token_data)
    );
  }

}
