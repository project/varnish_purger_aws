<?php

namespace Drupal\varnish_purger_aws\Plugin\Purge\DiagnosticCheck;

use Aws\Exception\AwsException;
use Drupal\varnish_purger_aws\Ec2Finder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckInterface;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckBase;
use Drupal\varnish_purger\Entity\VarnishPurgerSettings;

/**
 * Verifies that only fully configured Varnish purgers load.
 *
 * @PurgeDiagnosticCheck(
 *   id = "varnishconfiguration_aws",
 *   title = @Translation("Varnish AWS"),
 *   description = @Translation("Verifies that only fully configured Varnish AWS purgers load."),
 *   dependent_queue_plugins = {},
 *   dependent_purger_plugins = {"varnish_aws"}
 * )
 */
class ConfigurationCheck extends DiagnosticCheckBase implements DiagnosticCheckInterface {

  /**
   * @var \Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface
   */
  protected $purgePurgers;

  /**
   * Constructs a \Drupal\purge\Plugin\Purge\DiagnosticCheck\PurgerAvailableCheck object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface $purge_purgers
   *   The purge executive service, which wipes content from external caches.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PurgersServiceInterface $purge_purgers, protected Ec2Finder $ec2Finder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->purgePurgers = $purge_purgers;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('purge.purgers'),
      $container->get('varnish_purger_aws.ec2_finder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function run() {

    // Load configuration objects for all enabled HTTP purgers.
    $plugins = [];
    foreach ($this->purgePurgers->getPluginsEnabled() as $id => $plugin_id) {
      if ($plugin_id == 'varnish_aws') {
        $plugins[$id] = VarnishPurgerSettings::load($id);
      }
    }

    $total_instances_aws = [];
    // Perform checks against configuration.
    $labels = $this->purgePurgers->getLabels();
    foreach ($plugins as $id => $settings) {
      $t = ['@purger' => $labels[$id]];
      foreach (['name', 'port', 'request_method', 'scheme'] as $f) {
        if (empty($settings->get($f))) {
          $this->recommendation = $this->t("@purger not configured.", $t);
          return self::SEVERITY_ERROR;
        }
      }

      $aws_settings = $settings->getThirdPartySettings('varnish_purger_aws') ?? [];
      if (empty($aws_settings)) {
        $this->recommendation = $this->t("@purger not configured.", $t);
        return self::SEVERITY_ERROR;
      }

      foreach (['aws_key', 'aws_secret', 'aws_region'] as $aws_key) {
        if (empty($aws_settings[$aws_key])) {
          $this->recommendation = $this->t("@purger not configured.", $t);
          return self::SEVERITY_ERROR;
        }
      }

      try {
        $ips = $this->ec2Finder->getEc2Ips($settings, TRUE);
        if (empty($ips)) {
          $this->value = $this->t('No servers found');
          $this->recommendation = $this->t('You have connected to AWS but no Servers with State "running" have been found. Check the filters.');
          return self::SEVERITY_WARNING;
        }

        $total_instances_aws = array_merge(array_keys($ips), $total_instances_aws);
      }
      catch (AwsException $e) {
        $this->value = $e->getAwsErrorType();
        $this->recommendation = $e->getAwsErrorMessage();
        return self::SEVERITY_ERROR;
      }

      if (($settings->scheme === 'https') && ($settings->port != 443)) {
        $this->recommendation = $this->t("@purger uses https:// but its port is not 443!", $t);
        return self::SEVERITY_WARNING;
      }
      if (($settings->scheme === 'http') && ($settings->port == 443)) {
        $this->recommendation = $this->t("@purger uses http:// but its port is 443!", $t);
        return self::SEVERITY_WARNING;
      }
    }

    $total_instances_aws = array_unique($total_instances_aws);
    $this->value = $this->t('All purgers configured.');
    $this->recommendation = $this->t("EC2 founded: @instances", [
      "@instances" => implode(', ', $total_instances_aws),
    ]);
    return self::SEVERITY_OK;
  }

}
