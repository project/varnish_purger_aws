<?php

namespace Drupal\varnish_purger_aws;

use Aws\Ec2\Ec2Client;
use Aws\Exception\AwsException;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactory;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\varnish_purger\Entity\VarnishPurgerSettings;

/**
 *
 */
class Ec2Finder {

  const EC2_STATE_RUNNING = 'running';

  const CACHE_TIME_IPS = 3600;

  protected KeyValueStoreExpirableInterface $keyValueExpirable;

  /**
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactory $key_value_expirable_factory
   */
  public function __construct(KeyValueExpirableFactory $key_value_expirable_factory) {
    $this->keyValueExpirable = $key_value_expirable_factory->get('varnish_purger_aws');
  }

  /**
   * @param \Drupal\varnish_purger\Entity\VarnishPurgerSettings $settings
   * @param bool $throw_exception
   *
   * @return array
   */
  public function getEc2Ips(VarnishPurgerSettings $settings, bool $throw_exception = FALSE): array {
    $ips = $this->keyValueExpirable->get($settings->id() . '_ips', []);
    if (!empty($ips)) {
      return $ips;
    }
    $ips = [];

    $aws_key = $settings->getThirdPartySetting('varnish_purger_aws', 'aws_key');
    $aws_secret = $settings->getThirdPartySetting('varnish_purger_aws', 'aws_secret');
    $aws_region = $settings->getThirdPartySetting('varnish_purger_aws', 'aws_region');

    if (!$aws_key || !$aws_secret || !$aws_region) {
      return $ips;
    }

    $ec2Client = new Ec2Client([
      'region' => $aws_region,
      'version' => 'latest',
      'credentials' => [
        'key' => $aws_key,
        'secret' => $aws_secret,
      ],
    ]);

    $filters = [];
    foreach ($settings->getThirdPartySetting('varnish_purger_aws', 'aws_filters', []) as $filter) {
      $filters[$filter['field']][] = $filter['value'];
    }

    $filters_aws = [];
    if (!empty($filters)) {
      foreach ($filters as $name => $values) {
        $filters_aws[] = [
          'Name' => $name,
          'Values' => $values,
        ];
      }
    }

    try {
      $describe_instances_argument = [];
      if (!empty($filters_aws)) {
        $describe_instances_argument['Filters'] = $filters_aws;
      }
      $result = $ec2Client->describeInstances($describe_instances_argument);

      if (!empty($result['Reservations'])) {
        foreach ($result['Reservations'] as $reservation) {
          foreach ($reservation['Instances'] as $instance) {
            if ($instance['State']['Name'] == self::EC2_STATE_RUNNING) {
              $ips[$instance['InstanceId']] = $instance['PublicIpAddress'] ?? $instance['PrivateIpAddress'];
            }
          }
        }
      }
    }
    catch (AwsException $e) {
      if ($throw_exception) {
        throw $e;
      }
    }

    $this->keyValueExpirable->setWithExpire($settings->id() . '_ips', $ips, self::CACHE_TIME_IPS);

    return $ips;
  }

}
