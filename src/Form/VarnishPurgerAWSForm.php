<?php

namespace Drupal\varnish_purger_aws\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\varnish_purger\Entity\VarnishPurgerSettings;
use Drupal\varnish_purger\Form\VarnishPurgerForm;

/**
 *
 */
class VarnishPurgerAWSForm extends VarnishPurgerForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $settings = VarnishPurgerSettings::load($this->getId($form_state));
    $form['aws'] = [
      '#type' => 'details',
      '#group' => 'tabs',
      '#title' => $this->t('AWS'),
      '#description' => $this->t('In this section you configure the AWS.'),
      '#weight' => -10,
      '#tree' => TRUE,
    ];
    $form['aws']['aws_key'] = [
      '#title' => $this->t('Key'),
      '#type' => 'textfield',
      '#default_value' => $settings->getThirdPartySetting('varnish_purger_aws', 'aws_key', ''),
    ];
    $form['aws']['aws_secret'] = [
      '#title' => $this->t('Secret'),
      '#type' => 'textfield',
      '#default_value' => $settings->getThirdPartySetting('varnish_purger_aws', 'aws_secret', ''),
    ];
    $form['aws']['aws_region'] = [
      '#title' => $this->t('Region'),
      '#type' => 'textfield',
      '#default_value' => $settings->getThirdPartySetting('varnish_purger_aws', 'aws_region', ''),
    ];
    $form['aws']['aws_filters'] = [
      '#type' => 'table',
      '#caption' => $this->t('Filters EC2'),
      '#header' => [$this->t('Name'), $this->t('Value')],
      '#prefix' => '<div id="aws-filters-wrapper">',
      '#suffix' => '</div>',
    ];

    $aws_filters = $settings->getThirdPartySetting('varnish_purger_aws', 'aws_filters', []);
    if (is_null($form_state->get('aws_filters_items_count'))) {
      $value = empty($aws_filters) ? 1 : count($aws_filters);
      $form_state->set('aws_filters_items_count', $value);
    }
    for ($i = 0; $i < $form_state->get('aws_filters_items_count'); $i++) {
      if (!isset($form['aws']['aws_filters'][$i])) {
        $filter = $aws_filters[$i] ?? ['field' => '', 'value' => ''];
        $form['aws']['aws_filters'][$i]['field'] = [
          '#type' => 'textfield',
          '#default_value' => $filter['field'],
          '#attributes' => ['style' => 'width: 100%;'],
        ];
        $form['aws']['aws_filters'][$i]['value'] = [
          '#type' => 'textfield',
          '#default_value' => $filter['value'],
          '#attributes' => ['style' => 'width: 100%;'],
        ];
      }
    }
    $form['aws']['add_aws_filter'] = [
      '#type' => 'submit',
      '#name' => 'add_aws_filter',
      '#value' => $this->t('Add Filter'),
      '#submit' => [[$this, 'addAWSFilterSubmit']],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'addAWSFilterRebuild'],
        'wrapper' => 'aws-filters-wrapper',
        'effect' => 'fade',
      ],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addAWSFilterSubmit(array &$form, FormStateInterface $form_state) {
    $count = $form_state->get('aws_filters_items_count');
    $count++;
    $form_state->set('aws_filters_items_count', $count);
    $form_state->setRebuild();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function addAWSFilterRebuild(array &$form, FormStateInterface $form_state) {
    return $form['aws']['aws_filters'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormRequest(array &$form, FormStateInterface $form_state, VarnishPurgerSettings $settings) {
    parent::buildFormRequest($form, $form_state, $settings);
    $form['request']['hostname']['#disabled'] = TRUE;
    $form['request']['hostname']['#description'] = $this->t('Automatically searched by AWS SDK');
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    if (!is_null($submitted_aws_filters = $form_state->getValue(['aws', 'aws_filters']))) {
      $filters = [];
      foreach ($submitted_aws_filters as $filter) {
        if (strlen($filter['field'] && strlen($filter['value']))) {
          $filters[] = $filter;
        }
      }
      $form_state->setValue(['aws', 'aws_filters'], $filters);
    }
    $form_state->unsetValue(['aws', 'add_aws_filter']);
    $aws_settings = $form_state->getValue('aws');
    $form_state->unsetValue('aws');

    parent::submitFormSuccess($form, $form_state);
    $settings = VarnishPurgerSettings::load($this->getId($form_state));
    foreach ($aws_settings as $key => $value) {
      $settings->setThirdPartySetting('varnish_purger_aws', $key, $value);
    }
    $settings->save();
  }

}
