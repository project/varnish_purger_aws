# Introduction

Varnish Purger module extension to identify servers built on AWS, specifically using EC2.

By entering the key, secret key, region and a group of filters, the EC2 that are operational will be identified and the
Public or Private IP will be automatically extracted.

# Extension Varnish Purger Settings

This module extend the schema `varnish_purger.settings.*`. Adding the `aws` sequence attribute.

# Installation

This module requires you to have the AWS SDK.

```
composer require aws/aws-sdk-php:~3.0
```

# Configuration

Create a new Purger called Varnish Purger AWS. Define the credentials, the region and add a set of filters to identify
EC2s that use Varnish, for example `tag:Application` and the value `Varnish`, It will search for EC2s that have the tag
`Application`.

You can use the `purger_ui` module to view the status of the integration.
